;; code that should be run at the very beginning of init, e.g.


(radian-local-on-hook before-straight

  ;; code that should be run right before straight.el is bootstrapped,
  ;; e.g.
  )

(radian-local-on-hook after-init

  ;; code that should be run at the end of init, e.g.

  ;;; Set $PATH in emacs so the $GOPATH is known

  ;;;; Modifired emacs launcher
  ;;;; (emacs-init-time: 0.767368243 seconds)
  ;;;; launch emacs from default shell which already has $PATH set
  ;;;; correctly.
  ;;;; This will ensure that your `$PATH` is set correectly in emacs
  ;;;; while also maintaining a fast `emacs-init-time` of < 1 second
  ;;;; If you wish to run emacs in a desktop environment, edit the
  ;;;; emacs launcher by replacing `emacs %F` with the command below:
  ;"/bin/sh -c '$($SHELL --login -i -c 'emacs')'"

  ;;;; exec-path-from-shell - Make Emacs use the $PATH set up by the user's shell
  ;;;; https://github.com/purcell/exec-path-from-shell
  ;;;; (emacs-init-time: 2.213726091 seconds)
  ;(use-package exec-path-from-shell
    ;:init (exec-path-from-shell-initialize))

  ;;;; predacessor to exec-path-from-shell
  ;;;; (emacs-init-time: 2.139741212 seconds, requires `~/.bashrc` to set path)
  ;;;; source https://stackoverflow.com/questions/8606954/path-and-exec-path-set-but-emacs-does-not-find-executable/8609349#8609349
  ;(defun set-exec-path-from-shell-PATH ()
  ;"Set up Emacs' `exec-path' and PATH environment variable to match that used by the user's shell.
  ;This is particularly useful under Mac OSX, where GUI apps are not started from a shell."
  ;(interactive)
  ;(let ((path-from-shell (replace-regexp-in-string "[ \t\n]*$" ""
  ;(shell-command-to-string "$SHELL -c 'source ~/.bashrc && /bin/echo $PATH'"))))
    ;(setenv "PATH" path-from-shell)
    ;(setq exec-path (split-string path-from-shell path-separator))))
  ;(set-exec-path-from-shell-PATH)

  ;;; gorepl-mode - A minor emacs mode for Go REPL.
  ;;; https://github.com/manute/gorepl-mode
  (use-package gorepl-mode
    :defer t
    :commands (gorepl-run)
    ;; default setup mapping (this will override `go-goto-map')
    :hook (go-mode-hook . gorepl-mode))
 )

;; see M-x customize-group RET radian-hooks RET for which hooks you
;; can use with `radian-local-on-hook'
