# Install radian (emacs only) on arch linux

> note that this requires bash and is not POSIX or dash compatiple

```sh
bash install-radian-emacs.sh
```

After the above script runs, launch emacs with the following command:

> This will ensure that your `$PATH` is set correectly in emacs while
> also maintaining a fast `emacs-init-time` of < 1 second.
>
> If you wish to run emacs in a desktop environment, edit the emacs
> launcher by replacing `emacs %F` with the command below


```sh
/bin/sh -c '$($SHELL --login -i -c 'emacs')'
```
